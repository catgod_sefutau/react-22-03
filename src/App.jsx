import React, { Component } from 'react';
import logo from './logo.svg';
import List from './components/List.jsx';
import API from './helpers/fakeAPI.js';
import './App.css';

class App extends Component {
  constructor(props) {
    super(props);

    API.getTaskLists().then((param) => {
      this.lists = param;
      this.forceUpdate();
    });
  }
  
  render() {
    // const lists = this.lists;
    // randul de sub e echivalent cu cel de deasupra
    const {lists} = this.lists? this: {lists:[]};

    return (
      <div className="App">
        {lists.map( (myCurrentList) => {
          return <List list={myCurrentList}></List>
        } )}
      </div>
    );
  }
}

export default App;
