import React, { Component } from 'react';
import Task from './Task.jsx';
import TaskController from './TaskController.jsx';
import API from '../helpers/fakeAPI.js';

class List extends Component {
  constructor(props) {
    super(props);
  }

  addTaskToList = (taskName) => {
    const {list} = this.props;

    API.addTask(list.title, taskName, '')
    .then(() => {
      this.forceUpdate();
    })
    .catch(() => {
      alert("Mlem")
    });
  }

  render() {
    const {list} = this.state ? this.state: this.props;
    const {addTaskToList} = this;

    return (
      <div className="list-component">
        <h1>{list.title}</h1>
        <TaskController addTask={addTaskToList} listName={list.title}></TaskController>
        <ul>
        {list.taskArray.map( (myCurrentTask) => {
          return <Task task={myCurrentTask}></Task>
        })}
        </ul>
      </div>
    );
  }
}

export default List;
