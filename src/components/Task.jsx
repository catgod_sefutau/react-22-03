import React, { Component } from 'react';
import TaskDumb from './TaskDumb.jsx';

class Task extends Component {
  constructor(props) {
    super(props);
    
    this.state = {hasInitialized: false};
  }
  
  toggleCheck = () => {
    const {task} = this.props;
    if (this.state.checked == undefined){
      this.setState({checked: !task.checked});
    } else {
      this.setState({checked: !this.state.checked});
    }
  }

  render() {
    const {task} = this.props;
    const {toggleCheck} = this;
    var {checked} = task;

    if (this.state.checked != undefined) {
      checked = this.state.checked;
    }

    var propsObject = {
      task,
      checked,
      toggleCheck
    }

    return (<TaskDumb {...propsObject}></TaskDumb>);
  }
}

export default Task;
