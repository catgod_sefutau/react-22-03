import React, { Component } from 'react';

class TaskController extends Component {
  constructor(props) {
    super(props);

    this.taskName = '';
  }
  
  valueChanged = (newValue) => {
    this.taskName = newValue;
  }

  submitFunction = () => {
    const {addTask} = this.props;
    const {taskName} = this;
    
    addTask(taskName);
  }

  render() {
    const {valueChanged, submitFunction} = this;
    
    return (
      <div className="task-controller-component">
        <input name="task-name" onChange={(event) => { this.valueChanged(event.target.value) }}></input>
        <a href="#" onClick={submitFunction}>OK</a>
      </div>
    );
  }
}

export default TaskController;
