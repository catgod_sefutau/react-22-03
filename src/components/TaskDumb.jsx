import React, { Component } from 'react';

class TaskDumb extends Component {
  render() {
    const {task, checked, toggleCheck} = this.props;

    return (
      <div className="task-component" onClick={toggleCheck}>
        <h5>{task.title}</h5>
        Verificat: {checked?"Da":"Nu"}
      </div>
    );
  }
}

export default TaskDumb;
